import UserRegistrationForm from "./component/UserRegistrationForm";

function App() {
  return (
    <div className="App">
      <UserRegistrationForm />
    </div>
  );
}

export default App;
