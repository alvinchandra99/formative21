import React, { Component } from "react";

const Label = (props) => {
  return <p> {props.name} </p>;
};

const Input = (props) => {
  return (
    <input
      type={props.type}
      name={props.name}
      className="form-control"
      onChange={props.onChange}
    ></input>
  );
};

export default class UserRegistrationForm extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      date: "",
      zodiac: "",
    };

    this.postData = this.postData.bind(this);
  }

  postData() {
    let data = {
      name: this.state.name,
      birthdate: this.state.date,
    };

    let options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };

    fetch("http://localhost:8080/api/save", options)
      .then((res) => res.text())
      .then((data) => {
        this.setState({
          zodiac: data,
        });
      });
  }

  onChangeName(event) {
    this.setState({
      name: event.target.value,
    });
  }

  onChangeDate(event) {
    this.setState({
      date: event.target.value,
    });
  }

  render() {
    return (
      <div className="container mt-5" style={{ width: "450px" }}>
        <div className="row gy-5 mb-5">
          <div className="col-6">
            <Label name="Nama" />
          </div>
          <div className="col-6">
            <Input
              name="nama"
              type="text"
              onChange={(event) => this.onChangeName(event)}
            />
          </div>
          <div className="col-6">
            <Label name="Tanggal Lahir" />
          </div>
          <div className="col-6">
            <Input
              name="birthdate"
              type="date"
              onChange={(event) => this.onChangeDate(event)}
            />
          </div>
        </div>
        <button className="btn btn-primary" onClick={this.postData}>
          Submit
        </button>
        <div class="container-fluid bg-info mt-5 text-center">
          <Label name={this.state.zodiac}></Label>
        </div>
      </div>
    );
  }
}
