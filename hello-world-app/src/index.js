import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';



const HelloWorld = ()=>{
  return (
  <div class="container mt-5 bg-light text-center">
  
   <h1> Hello World</h1> 
   <h5> {new Date().toLocaleDateString()} </h5>
  
  </div>
  );
}

ReactDOM.render(
  <React.StrictMode>
  <HelloWorld/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
