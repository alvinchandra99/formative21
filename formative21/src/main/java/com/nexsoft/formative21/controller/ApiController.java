package com.nexsoft.formative21.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.nexsoft.formative21.model.User;
import com.nexsoft.formative21.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    UserService userService;

    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<String> save(@RequestBody User user) {

        System.out.println(user.getName());

        System.out.println(user.getBirthdate());

        LocalDate birthdate = LocalDate.parse(user.getBirthdate());
        int day = birthdate.getDayOfMonth();
        int month = birthdate.getMonthValue();
        String zodiac = getZodiac(month, day);

        user.setZodiac(zodiac);

        userService.save(user);

        return new ResponseEntity<>(zodiac, HttpStatus.OK);
    }

    public String getZodiac(int month, int day) {

        if ((month == 12 && day >= 22) || (month == 1 && day <= 20)) {
            return "Capricorn";
        }
        if ((month == 1 && day >= 21) || (month == 2 && day <= 19)) {
            return "Aquarius";
        }
        if ((month == 2 && day >= 20) || (month == 3 && day <= 20)) {
            return "pisces";
        }
        if ((month == 3 && day >= 21) || (month == 4 && day <= 19)) {
            return "Aries";
        }
        if ((month == 4 && day >= 20) || (month == 5 && day <= 20)) {
            return "Taurus";
        }
        if ((month == 5 && day >= 21) || (month == 6 && day <= 21)) {
            return "Gemini";
        }
        if ((month == 6 && day >= 22) || (month == 7 && day <= 23)) {
            return "Cancer";
        }
        if ((month == 7 && day >= 24) || (month == 8 && day <= 23)) {
            return "Leo";
        }
        if ((month == 8 && day >= 24) || (month == 9 && day <= 22)) {
            return "Virgo";
        }
        if ((month == 9 && day >= 23) || (month == 10) && day <= 22) {
            return "Libra";
        }
        if ((month == 10 && day >= 23) || (month == 11 && day <= 22)) {
            return "Scorpio";
        }
        if ((month == 11 && day <= 23) || (month == 12 && day <= 20)) {
            return "Sagitarius";
        }

        return "";
    }

}