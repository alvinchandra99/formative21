package com.nexsoft.formative21.service;

import javax.transaction.TransactionScoped;

import com.nexsoft.formative21.model.User;
import com.nexsoft.formative21.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class UserService {

    @Autowired
    UserRepository userRepository;

    public void save(User user){
        userRepository.save(user);
    }
}
