package com.nexsoft.formative21;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Formative21Application {

	public static void main(String[] args) {
		SpringApplication.run(Formative21Application.class, args);
	}

}
